/**
 * This module manage mockData services
 * Author : Pagano Francesco
 * Decisyon Srl
 */
(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name dcyApp.dcyMockDataService
   *
   * @requires $routeParams
   * @requires dcyConstants
   * @requires $http
   * @requires $q
   * @requires $log
   *
   * @description
   * Factory for Manage mock data resources
   *
   * @example
   * ```js
   * function dpFactoryFn(dcyMockDataFactory){
   *    var mockDataDefferer = dcyMock
   *    DataFactory({key:value});
   * }
   * myFn.$inject = ['dcyMockDataFactory'];
   *
   * angular.module('dcyApp.factories')
   *     .factory('dpFactory', dpFactoryFn);
   *
   * ```
   */
  function MockDataService(dcyConstants, $http, $q, $log, dcyCommonFnFactory, dcyUtilsFactory) {

    /**
     * @ngdoc method
     * @name responseObjectHttpRequest
     * @methodOf dcyApp.dcyMockDataService
     *
     * @description
     *  Manage object that return for $http request response
     *
     * @param {object} response data from $http call.
     * @param {integer} status from $http call
     * @param {object} headers from $http call
     * @param {object} config from $http call
     * @returns {*} wrapper response from $http call.
     */
    function responseObjectHttpRequest(response, status, headers, config) {
      return {
        response: response,
        status: status,
        headers: headers,
        config: config
      };
    }

    /**
     * @ngdoc method
     * @name validateDpMock
     * @methodOf dcyApp.dcyMockDataService
     *
     * @description
     * Check if dpcItem have a valid structure for get mock data
     *
     * @param {object} dpcItem object.
     * @returns {boolean} true if dpcItem is valid.
     */
    function validateDpMock(dpcItem) {
      var matches = {
          'js': '',
          'json': '',
          'url': ''
        },
        countMatches = 0;

      angular.forEach(dpcItem, function(value, key) {
        if (angular.isDefined(matches[key])) {
          countMatches++;
        }
      });

      return (countMatches === 1);
    }

    /**
     * @ngdoc method
     * @name effectiveLogicToCall
     * @methodOf dcyApp.dcyMockDataService
     *
     * @description
     * This is a factory that prepares the actual logic to be invoked based on the type of resource set
     *
     * @param {string} sourceType object.
     * @returns {object} sourceType and executeFunction for actual resource
     */
    function effectiveLogicToCall(sourceType) {

      var mainInterface = {
        sourceType: sourceType,
        executeFn: function() {}
      };

      if (angular.isDefined(sourceType) && !angular.equals(sourceType, '')) {
        switch (sourceType) {
          case 'js':
            mainInterface.executeFn = function(responseData, matchItem, httpParams) {
            

              var data = {},
                functToCall = angular.isDefined(matchItem.functData) ? matchItem.functData + '();' : 'getData(httpParams);',
                mainJsSource = 'function () { ' + responseData + ' return ' + functToCall + '}',
                mockDataCustomFunctions = eval('(' + mainJsSource + ')');
              	
              /*MM: si potrebbe sostituire con questo?
               	var functionVariable = new Function ('return '+mainJsSource);
				functionVariable (); 
               * */
              data = mockDataCustomFunctions();

              if ((data && data.then) || dcyCommonFnFactory.responseValidator(data)) {
                return data;
              }

              $log.error('dcyMockDataService#effectiveLogicToCall# <DCY_ERROR>: your function not return an object data');
              return null;

             
            };
            break;

          case 'json':
            mainInterface.executeFn = function(responseData) {
              if (dcyCommonFnFactory.responseValidator(responseData)) {
                return responseData;
              }

              $log.error('dcyMockDataService#effectiveLogicToCall#<DCY_ERROR>: your function not return an object data');
              return null;
            };
            break;

          case 'url':
            mainInterface.executeFn = function(responseData) {
              if (dcyCommonFnFactory.responseValidator(responseData)) {
                return responseData;
              }

              $log.error('dcyMockDataService#effectiveLogicToCall# <DCY_ERROR>: your function not return an object data');
              return null;
            };

            break;
        }
      }

      return mainInterface;
    }

    /**
     * @ngdoc method
     * @name getResourceInfoPath
     * @methodOf dcyApp.dcyMockDataService
     *
     * @description
     * Get from items Matched of mappingFile the path for http request
     *
     * @param {object} key object.
     * @returns {object} infoPath that contains sourceType resource and data path for http request
     */
    function getResourceInfoPath(key) {
      /**
       * Check whether the parameters you have defined a file mockdata want to call;
       * In this case resolve the response directly returned the Absolute Paths for this mockdata file
       */
      var resourceInfo = dcyUtilsFactory.isValidString(key.json) ? {
          sourceType: 'json',
          sourceData: key.json
        } :
        dcyUtilsFactory.isValidString(key.js) ? {
          sourceType: 'js',
          sourceData: key.js
        } :
        dcyUtilsFactory.isValidString(key.url) ? {
          sourceType: 'url',
          sourceData: key.url
        } : undefined;

      if (dcyUtilsFactory.isValidObject(resourceInfo)) {
        resourceInfo.sourceData = dcyUtilsFactory.getMockDataPath(resourceInfo.sourceData, resourceInfo.sourceType);
      }

      return resourceInfo;
    }
    
    /**
     * @ngdoc method
     * @name validateCtxNode
     * @methodOf dcyApp.dcyMockDataService
     *
     * @description
     * Validate ctx node of mappingFile with ctx node setted in orgininal params
     *
     * @param {object} originalCtxParams object.
     * @param {object} MatchedCtxParams object.
     * @returns {boolean} true if match
     */
    function validateCtxNode(originalCtxParams, MatchedCtxParams) {
      var isMatch = false,
        checkCtxMatching = false,
        isOrigCtxValid = dcyUtilsFactory.isValidObject(originalCtxParams),
        isMatchedCtxValid = dcyUtilsFactory.isValidObject(MatchedCtxParams);

      checkCtxMatching = angular.isUndefined(originalCtxParams) && angular.isUndefined(MatchedCtxParams) ? true : checkCtxMatching;
      checkCtxMatching = !isOrigCtxValid && !isMatchedCtxValid ? true : checkCtxMatching;
      checkCtxMatching = isOrigCtxValid && isMatchedCtxValid ? true : checkCtxMatching;
      checkCtxMatching = angular.isUndefined(originalCtxParams) && !dcyUtilsFactory.isValidObject(MatchedCtxParams) ? true : checkCtxMatching;
      checkCtxMatching = angular.isUndefined(MatchedCtxParams) && !isOrigCtxValid ? true : checkCtxMatching;
      

      if (checkCtxMatching === true) {
        isMatch = true;
      } else if (dcyUtilsFactory.isValidObject(originalCtxParams) && dcyUtilsFactory.isValidObject(MatchedCtxParams)) {
        /**Check that all the items specified in the ctx meet the dpc taken from the json response*/
        angular.forEach(MatchedCtxParams, function(value, key) {
          isMatch = angular.equals(originalCtxParams[key], value);
        });
      }

      return isMatch;
    }

    /**
     * @ngdoc method
     * @name getData
     * @methodOf dcyApp.dcyMockDataService
     *
     * @description
     * Get Mock Data response
     *
     * @param {object} requestParams object.
     * @param {string} sourceType object.
     * @param {object} itemsMatched object.
     * @param {object} originalHttpParams object.
     * @returns {object} promise deferer
     */
    function getData(requestParams, infoPath, itemsMatched, originalHttpParams) {

      var localDeferer = $q.defer();

      function successHandler(response) {

        var getLogic = effectiveLogicToCall(infoPath.sourceType),
          //Original Data
          data = getLogic.executeFn(response, itemsMatched, originalHttpParams);

        if (dcyUtilsFactory.isValidObject(data)) {
          return localDeferer.resolve(data);
        }

        $log.error('dcyMockDataService#getData#successHandler# <DCY_ERROR>: your function not return an object data');
        return localDeferer.reject(data);
      }

      function errorHandler(response, status, headers, config) {
        $log.error('dcyMockDataService#getData#errorHandler# Error for HTTP request; error status : ', status, '-> with this response: ', response);
        return localDeferer.reject(responseObjectHttpRequest(response, status, headers, config));
      }

      if (dcyUtilsFactory.isValidObject(infoPath) && dcyUtilsFactory.isValidObject(requestParams)) {
        //Set directly url for http request and get data
        requestParams.url = infoPath.sourceData;

        $http(requestParams)
          .success(successHandler)
          .error(errorHandler);

        localDeferer.notify('dcyMockDataService# Http request started # Attend data response');
      }

      return localDeferer.promise;
    }

    /**
     * @ngdoc method
     * @name mockDataFactoryFn
     * @methodOf dcyApp.dcyMockDataService
     *
     * @description
     * Constructor Function return for factory
     *
     * @param {object} params params for $http request
     * @returns {*} false if mockdata not enabled or promise of $http request
     */
    return function mockDataFactoryFn(params) {

      //The service started if USE_MOCK_DATA constants is enabled === true
      if (dcyConstants.USE_MOCK_DATA !== true) {
        $log.info('dcyMockDataService#mockDataFactoryFn# Mock data is not enabled');
        return false;
      }

      var
      //Main deferer for this service
        deferred = $q.defer(),
        //Interface Object for http request on resources
        requestParams = {
          method: 'get',
          url: dcyConstants.MOCK_DATA_BASE_PATH + '/mockDataMapping.json', //Main resource for get mockData mapping
          cache: false
        };


      function getDataRequest(requestParams, path, itemsToMatch, originalHttpParams) {
        //Http call that return deferer
        var dataRequest = getData(requestParams, path, itemsToMatch, originalHttpParams);
        dataRequest.then(deferred.resolve, deferred.reject);
      }


      //If in original params passed contain mockData options
      if (dcyUtilsFactory.isValidString(params.mockdata)) {
        var resourceInfoPath = getResourceInfoPath({
          json: params.mockdata
        });
        getDataRequest(requestParams, resourceInfoPath, {}, params);
        //Return main defere promise
        return deferred.promise;
      }

      /**
       * Check whether the parameters you have defined a file dpc value;
       */
      if (!dcyUtilsFactory.isValidString(params.dpc)) {
        $log.error('dcyMockDataService#mockDataFactoryFn# Error : dpc is not valid');
        return false;
      }
      
      function actionToDoAfterResponse(response, itemsMatched){
    	  if (!validateDpMock(itemsMatched)) {
    		  $log.error('dcyMockDataService#successHandler# <DCY_ERROR>: your dataprovider declaration contains duplicate mockdata sources');
    		  return deferred.reject(response);
    	  }

    	  /**If ctx validation return true response -> match*/
    	  if (validateCtxNode(params.ctx, itemsMatched.ctx)) {
    		  var pathInfoForHttpRequest = getResourceInfoPath(itemsMatched);

    		  if (!dcyUtilsFactory.isValidObject(pathInfoForHttpRequest)) {
    			  return deferred.reject(response);
    		  } else {
    			  getDataRequest(requestParams, pathInfoForHttpRequest, {}, params);
    		  }

    		  return deferred.notify('dcyMockDataService#successHandler# Http request started # Attend data response #');
    	  }

    	  return deferred.reject(response);
      }
      
      function successHandler(response) {
        /**Check is response is a object*/
        if (dcyUtilsFactory.isValidObject(response)) {

          /**Get item information from json response*/
          var itemsMatched = response[params.dpc];

          /**Check if there is a specified dpc*/
          if (angular.isDefined(itemsMatched)) {

        	  return actionToDoAfterResponse(response, itemsMatched);

          }
          //No dpcItem found in main mock data file
          return deferred.reject(response);
        }
        $log.error('dcyMockDataService#successHandler# <DCY_ERROR>: ERROR ON LOADING MAIN MOCK DATA FILE -> its not an object');
        return deferred.reject(response);
      }

      function errorHandler(response, status, headers, config) {
        $log.error('dcyMockDataService#errorHandler# Error for HTTP request; error status :  ', status, '-> with this response ', response);
        return deferred.reject(responseObjectHttpRequest(response, status, headers, config));
      }

      $http(requestParams)
        .success(successHandler)
        .error(errorHandler);

      return deferred.promise;
    };
  }


  MockDataService.$inject = ['dcyConstants', '$http', '$q', '$log', 'dcyCommonFnFactory', 'dcyUtilsFactory'];

  /**Register factory on factory module of dcy */
  angular.module('dcyApp.services').service('dcyMockDataService', MockDataService);
}());
