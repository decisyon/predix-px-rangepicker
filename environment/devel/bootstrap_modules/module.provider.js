(function() {
	'use strict';
	  
	function DcyModule($controllerProvider, $provide, $compileProvider, $filterProvider, $injector) {

		 	
		var providers = {
			$controllerProvider	: $controllerProvider,
			$compileProvider	: $compileProvider,
			$filterProvider		: $filterProvider,
			$provide			: $provide,
			$injector			: $injector
		},
		
		/**
		 * Check if module passed exist and return our object function
		 * @param moduleName
		 */
		getModuleFn = function(moduleName) {  
			var moduleFn;
			try {
				moduleFn = angular.module(moduleName);
			} catch(e) {
				if(/No module/.test(e) || (e.message.indexOf('$injector:nomod') > -1)) {
					moduleFn = false;
				}
			}
			return moduleFn;
		},
		/**
		 * Get the list of required modules/services/... for this module
		 * @param module
		 * @returns {Array}
		 */
		getModuleDepsRequires = function(moduleName) {
			var depsFound = new Array();
			
			//@Private method
			var getDeps = function(moduleName){
				//Controllo che il nome del modulo passato sia una stringa valida
				if(typeof moduleName !== 'string' || moduleName === ''){
					throw 'Module with name '+ moduleName +'is not valid.';
				}
				
				//Controllo che il modulo sia registrato nel contesto 'Angular'
				var moduleFn = getModuleFn(moduleName);
				if(typeof moduleFn !== 'object'){
					throw 'Module with name '+ moduleName +'is not registered.';
				}
				
				//Ricorsivit�
				angular.forEach(moduleFn.requires, function(key, value){
					getDeps(key);
				});
				
				//Push del nuovo modelName che sto gestendo all'interno di un'array
				depsFound = _.union(depsFound, [moduleName]);
			};
			
			getDeps(moduleName);
			
			return depsFound;
		},
		
		/**
		 * Register a new module and load it and load all his dependencies
		 * @param {Array} registerModules
		 */
		register = function(registerModules, dcyUtilsFactory) {
			var moduleName, //Terr� traccia del nome del modulo che sto gestendo di volta in volta
				moduleFn, //Conterr� le api del modulo che sto gestendo di volta in volta
				toInvoke, //Conterr� i componenti da registrare del modulo che sto gestendo di volta in volta 
				provider; //Conterr� il provider giusto per la registrazione del componente del modulo che sto gestendo di volta in volta
			//Controllo che mi sia stato passato un array
			if(Object.prototype.toString.call(registerModules) === '[object Array]') {
				var runBlocks = new Array(); //Conterr� tutte le 'run phases' dei moduli gestiti 
				for(var i = 0; i < registerModules.length; i++) {
					moduleName = registerModules[i];
					moduleFn = getModuleFn(moduleName);
					//Controllo che il modulo sia valido
					if(typeof moduleFn === 'object'){
						//Mi salvo la potenziale 'run phase' del modulo che sto gestendo
						runBlocks = runBlocks.concat(moduleFn._runBlocks);
						//Racchiudo in un try-catch per gestire eventuali errori da parte dell'utente
						//(es. Sta tentando di registrare un componente x cui il provider non esiste)
						try {
							//Catturo la lista delle dipendenze (i componenti registrati sul modulo es. factories/directives etc..)
							var invokeQueue = moduleFn._invokeQueue;
							//Itero le dipendenze
							for(var j = 0; j < invokeQueue.length; j++) {
								//Seleziono la dipendenza 'j' dall'array
								toInvoke = invokeQueue[j];
								//Controllo che tra i providers ci sia il provide utilizzato dall'utente
								if(providers.hasOwnProperty(toInvoke[0])) {
									//Se esiste seleziono il relativo provider da richiamare
									provider = providers[toInvoke[0]];
								} else {
									return errorToConsole("unsupported provider " + toInvoke[0]);
								}
								//Del provider seleziono il sotto-provider da invocare a cui passo la funzione
								//definita dall'utente
								var providerName = angular.equals(toInvoke[0], '$controllerProvider') ? 'controller' : toInvoke[1];
								if(!dcyUtilsFactory.isComponentExist(providerName, toInvoke[2][0])){
									provider[toInvoke[1]].apply(provider, toInvoke[2]);
								}
							}
						} catch(e) {
							if(e.message) {
								e.message += 'from ' + moduleName;
							}
							errorToConsole(e.message);
							throw e;
						}
					}
				}
				//Itero i le eventuali run phase trovate e le invoco utilizzando l'injector
				angular.forEach(runBlocks, function(constructor) {
					if(angular.isFunction(constructor)){
						constructor = [constructor];
					}

					if(dcyUtilsFactory.isValidArray(constructor)){
						var fn 						= constructor.pop();
						var componentsNametoInject 	= constructor.length ? constructor : dcyUtilsFactory.isValidArray(fn.$inject) ? fn.$inject : null;
						var instancesToInject;
						
						if(dcyUtilsFactory.isValidArray(componentsNametoInject)){
							if(angular.equals(componentsNametoInject, _.uniq(componentsNametoInject))){
								instancesToInject = new Array();
								angular.forEach(componentsNametoInject, function(value, key){
									var instance = invokeAngularService(value);
									if(instance){
										instancesToInject.push(instance);
									}
								});
							}
							else{
								//Vi sono componenti injettati duplicati. Ci comportiamo come angular e quindi lanciamo eccezione.
								var message = 'There are duplicated components injected into run phase. The injection in error is : [' + angular.fromJson(componentsNametoInject) + ']';
								errorToConsole(message);
								throw message;
							}
						}
						
						try {
							fn.apply(this, instancesToInject);
						} catch (e) {
							//Nel caso il corpo della function di run phase generi errore logghiamo l'errore ma andiamo avanti con lesecuzione.
							errorToConsole(e.stack);
						}
					}
				});
			}
		};

		this.$inject = ['dcyUtilsFactory', '$timeout', '$q'];
		
		this.$get = ['dcyUtilsFactory','$timeout', '$q', function(dcyUtilsFactory, $timeout, $q) {
			return {
				registerModule: function(modulesName) {
					
					if(typeof modulesName === 'string') {
						modulesName = [modulesName];
					}
					
					if(Object.prototype.toString.call(modulesName) === '[object Array]'){
						for(var i=0; i < modulesName.length; i++){
							register(getModuleDepsRequires(modulesName[i]), dcyUtilsFactory);
						}
					}
					else{
						errorToConsole('Modules Name not valid. Expect Array or String.');
						return false;
					}
					
					return true;
				}			
			};
		}];

	};
	
	angular.module('dcyApp.providers').provider('dcyModule', ['$controllerProvider', '$provide', '$compileProvider', '$filterProvider', '$injector', DcyModule]);

}());
