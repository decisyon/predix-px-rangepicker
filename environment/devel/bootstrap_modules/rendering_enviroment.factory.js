(function() {
	'use strict';
	
	function RenderingEnviromentFactory() {

		var DECISYON = window.DECISYON_CORE;
		
		var resourcesPaths = new Object();
		
		function setResourcePath(logicalName, resourcePath) {
			if(!resourcesPaths[logicalName]){
				resourcesPaths[logicalName] = {
					resourcePath : resourcePath
				};
			}
		}
		
		function getResourcePathByLogicalName(logicalName){
			return resourcesPaths[logicalName].resourcePath;
		}
		
		function getResourcesPaths() {
			return resourcesPaths;
		}

		return {
			setResourcePath					: setResourcePath,
			getResourcesPaths				: getResourcesPaths,
			getResourcePathByLogicalName	: getResourcePathByLogicalName
		};
	}

	RenderingEnviromentFactory.$inject = [];
	angular.module('dcyApp.factories').factory('dcyRenderingEnviromentFactory', RenderingEnviromentFactory);
}());
