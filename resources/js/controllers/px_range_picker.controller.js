(function() {
	/**
	* Controller to manage the rangepicker widget.
	* link: https://www.predix-ui.com/?show=px-rangepicker&type=component
	*
	*/
	
	'use strict';
	
	var PxRangePickerCtrl = function($scope, $timeout, $element) {
		//Controller Fn
		var ctrl					= this;
		//Web component selector
		var $pxRangePicker			= $('px-rangepicker', $element)[0];
		//Target DCY Api
		var target					= $scope.DECISYON.target;
		//Widget context
		var ctx 					= target.content.ctx;
		//Widget data
		var data					= target.content.data;
		//Msh Page used to understand if we are into mshPage
		var mshPage					= target.page;
		//Base name assign to widget
		var BASE_NAME				= ctx.baseName.value;
		//StartDate constant 
		var PARAM_START_DATE		= BASE_NAME + '_StartDate';
		//StartDate in epoch constant 
		var PARAM_START_EPOCH_DATE	= BASE_NAME + '_StartDateEpoch';
		//EndDate constant 
		var	PARAM_END_DATE			= BASE_NAME + '_EndDate';
		//EndDate in epoch constant 
		var PARAM_END_EPOCH_DATE	= BASE_NAME + '_EndDateEpoch';
		//DEFAULT_TIMEZONE constant
		var DEFAULT_TIMEZONE		= 'AUTO';
		//Value for disable a property
		var DEFAULT_DISABLE_PROPERTY= 'false';
		//DEFAULT_TIMEFORMAT constant
		var DEFAULT_TIMEFORMAT		= 'HH:mm:ss A';
		//DEFAULT_TIMEFORMAT constant
		var DEFAULT_DATEFORMAT		= 'MM/DD/YYYY';
		//DEFAULT_TIMEFORMAT constant : default can't be set to show the timezone.. the timezone format is contained into $pxtimeZone properties
		var DEFAULT_SHOWTIMEZONE	= 'none';
		//EMPTY_PARAM_VALUE constant
		var EMPTY_PARAM_VALUE		= 'PARAM_VALUE_NOT_FOUND';
		//FORMAT DATE FOR EXPORT PARAM
		var FORMAT_DATE_PARAM_TO_EXPORT	= 'YYYYMMDD';
		//FORMAT UNIX TIMESTAMP
		var FORMAT_UNIX_TIMESTAMP		= 'x';
		
		//This routine initialize options for widget
		var manageOptions = function(context){
		    ctrl.pxblockFutureDates	= context.$pxblockFutureDates.value !== ''				? context.$pxblockFutureDates.value		: DEFAULT_DISABLE_PROPERTY;
			ctrl.pxblockPastDates	= context.$pxblockPastDates.value !== ''				? context.$pxblockPastDates.value		: DEFAULT_DISABLE_PROPERTY;
			ctrl.pxhidePresets		= context.$pxhidePresets.value !== ''					? context.$pxhidePresets.value			: DEFAULT_DISABLE_PROPERTY;
			ctrl.pxhideTime			= context.$pxhideTime.value !== ''						? context.$pxhideTime.value				: DEFAULT_DISABLE_PROPERTY;
			ctrl.pxtimeFormat		= context.$pxtimeFormat.value !== ''					? context.$pxtimeFormat.value			: DEFAULT_TIMEFORMAT;
			ctrl.pxtimeZone			= context.$pxtimeZone.value.value !== DEFAULT_TIMEZONE	? context.$pxtimeZone.value.value		: Px.moment.tz(Px.moment.tz.guess())._z.name;
			ctrl.pxshowTimeZone		= context.$pxshowTimeZone.value.value !== ''			? context.$pxshowTimeZone.value.value	: DEFAULT_SHOWTIMEZONE;
			ctrl.pxdateFormat		= context.$pxdateFormat.value !== ''					? context.$pxdateFormat.value			: DEFAULT_DATEFORMAT;
			ctrl.pxRange			= null;
			ctrl.showButtons		= 'true';
		};
		
		/* Function to set the parameters on the page. */
		var sendParams = function(range) {
			if (mshPage){
				//Send Start Date in both format
				target.sendParamChanged(PARAM_START_DATE , range.start.default);
				target.sendParamChanged(PARAM_START_EPOCH_DATE , range.start.epoch);
				//Send End Date in both format
				target.sendParamChanged(PARAM_END_DATE , range.end.default);
				target.sendParamChanged(PARAM_END_EPOCH_DATE , range.end.epoch);
			}
		};
		
		var sendRangeSet = function(startDate, endDate){
			var rangeToSet = {
				start : {
					default : Px.moment(startDate).format(FORMAT_DATE_PARAM_TO_EXPORT),
					epoch   : Px.moment.utc(startDate).format(FORMAT_UNIX_TIMESTAMP)
				},
				end : {
					default : Px.moment(endDate).format(FORMAT_DATE_PARAM_TO_EXPORT),
					epoch   : Px.moment.utc(endDate).format(FORMAT_UNIX_TIMESTAMP)
				}
			}; 
			//Send params 	
			sendParams(rangeToSet);
			adjustCalendarPannels(startDate, endDate);
		};
		
		var applyDomListeners = function(){
			$pxRangePicker.addEventListener('px-datetime-button-clicked', function(e) {
				if(this.$.field.isValid){
					sendRangeSet(this.fromMoment.toISOString(), this.toMoment.toISOString());
				}
			});
			
			//px-cell-blured, px-moment-changed, px-cell-validate
			$pxRangePicker.addEventListener('px-cell-blured', function(e) {
				if(!this._opened && this.$.field.isValid){
					sendRangeSet(this.fromMoment.toISOString(), this.toMoment.toISOString());
				}
			});
		};
		
		/* Check imported parameters */
		var getImportedParamFromContext = function(importedParam) {
			return (angular.isDefined(importedParam) && !angular.equals(importedParam.value, EMPTY_PARAM_VALUE)) ? importedParam.value : '';
		};
		
		/* Managing of dates according to date epoch parameter  */
		var getDateEpochToImport = function(dateEpoch) {
			return Px.moment(dateEpoch, FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Managing of dates according to date parameter  */
		var getDateToImport = function(date) {
			return Px.moment(Px.moment(date, FORMAT_DATE_PARAM_TO_EXPORT).utc().format(FORMAT_UNIX_TIMESTAMP), FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Managing of imported dates */
		var getValidDatesToImport = function(pDate, pDateEpoch){
			var isValidDate = Px.moment(pDate,FORMAT_DATE_PARAM_TO_EXPORT).isValid(),
				isValidDateEpoch = parseInt(pDateEpoch) && Px.moment(pDateEpoch, FORMAT_UNIX_TIMESTAMP).isValid(),
				dateToYYYYMMDD = (isValidDate) ? parseInt(Px.moment(pDate).format(FORMAT_DATE_PARAM_TO_EXPORT)) : '',
				dateEpochToYYYYMMDD = (isValidDateEpoch) ? parseInt(Px.moment(new Date(pDateEpoch)).format(FORMAT_DATE_PARAM_TO_EXPORT)) : '',
				isSameDate = (dateToYYYYMMDD === dateEpochToYYYYMMDD),
				useDateEpoch = isValidDateEpoch && (!ctrl.pxhideTime || isSameDate),
				useDate = isValidDate && (ctrl.pxhideTime || !isValidDateEpoch);
			return (useDateEpoch) ? getDateEpochToImport(pDateEpoch) : (useDate ? getDateToImport(pDate) : Px.moment().toISOString());
		};
		
		/* Return the date in input in UNIX Timestamp milliseconds format */
		var getDataInUnixTimestamp = function(pDate) {
			return parseInt(Px.moment(pDate).format(FORMAT_UNIX_TIMESTAMP));
		};
		
		/* Return the date in input in YYYYMMDD format */
		var getDateInYYYYMMDD = function(pDate) {
			return parseInt(Px.moment(pDate).format(FORMAT_DATE_PARAM_TO_EXPORT));
		};
		
		/* Return the date after adding 7 days from date in input */
		var getDateAfterAddingSevenDays = function(pDate) {
			return Px.moment(Px.moment(pDate).add(7,'day').format(FORMAT_UNIX_TIMESTAMP), FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Return the date after subtracting 7 days from date in input */
		var getDateAfterSubtractingSevenDays = function(pDate) {
			return Px.moment(Px.moment(pDate).subtract(7,'day').format(FORMAT_UNIX_TIMESTAMP), FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Return the valide date by adding or substracting 7 days if necessary */
		var getValidDateForRange = function(isDateToChange, originalDate, validDateToSet, canAddDays) {
			if (isDateToChange){
				return canAddDays ? getDateAfterAddingSevenDays(validDateToSet) : getDateAfterSubtractingSevenDays(validDateToSet);
			}
			return originalDate;
		};
		
		var adjustCalendarGui = function(calendar, dateToSet){
			var newMoment;
			var actualDate   = parseInt(calendar.baseDate.format("M"));
			if(actualDate > dateToSet){
				newMoment = Px.moment.tz(calendar.baseDate.subtract(actualDate - dateToSet, 'months'), calendar.timeZone);
			}
			else{
				newMoment = Px.moment.tz(calendar.baseDate.add(dateToSet - actualDate, 'months'), calendar.timeZone);
			}
			calendar.set('baseDate', newMoment);
			calendar._renderCurrent();
		};
		
		
		var adjustCalendarPannels = function(startDate, endDate){
			$scope.$applyAsync(function(){
				var calendarFrom    = $('px-calendar-picker[id="from"]', $element)[0];
				var calendarTo      = $('px-calendar-picker[id="to"]', $element)[0];
				
				var numberOfMonthStart  = parseInt(Px.moment(startDate).format("M"));
				var numberOfMonthEnd    = parseInt(Px.moment(endDate).format("M"));
				
				adjustCalendarGui(calendarTo, numberOfMonthEnd);
				adjustCalendarGui(calendarFrom, numberOfMonthStart);
			});
		};
		
		var setRangeProperty = function(startDate, endDate){
			ctrl.pxRange = {
				"from"	:	startDate,
				"to"	:	endDate
			};
			adjustCalendarPannels(startDate, endDate);
		};
		
		/* Managing of both the param start-date and the param end-date.
		- if both the past dates and the future dates are blocked, use today's date
		- if the past dates aren't blocked, substract 7 days from param start-date
		- if the past dates are blocked and the future dates aren'blocked, add 7 days to param end-date
		*/
		var manageRangeProperty = function(startDate, endDate) {
			var todayDate						= Px.moment().toISOString(),
				todayDateInYYYYMMDD				= getDateInYYYYMMDD(todayDate),
				isSameDate						= getDateInYYYYMMDD(startDate) === getDateInYYYYMMDD(endDate),
				isStartDateGreaterThanToday		= getDateInYYYYMMDD(startDate) > todayDateInYYYYMMDD,
				isStartDateLessThanToday		= getDateInYYYYMMDD(startDate) < todayDateInYYYYMMDD,
				isEndDateGreaterThanToday		= getDateInYYYYMMDD(endDate) > todayDateInYYYYMMDD,
				isEndDateLessThanToday			= getDateInYYYYMMDD(endDate) < todayDateInYYYYMMDD,
				isStartDateGreterThanEndDate	= getDateInYYYYMMDD(startDate) > getDateInYYYYMMDD(endDate),
				validStartDate					= startDate,
				validEndDate					= endDate;

			if (ctrl.pxblockPastDates && ctrl.pxblockFutureDates){
				validStartDate = todayDate;
				validEndDate = todayDate;
			}else {
				if (!ctrl.pxblockPastDates && !ctrl.pxblockFutureDates){
					validStartDate  = getValidDateForRange(isSameDate, startDate, todayDate, false);
					validEndDate    =  getValidDateForRange(isStartDateGreterThanEndDate, endDate, startDate, true);
				}
				else if (ctrl.pxblockPastDates && !ctrl.pxblockFutureDates){
					if (isStartDateLessThanToday){
						validStartDate = todayDate;
						validEndDate = getValidDateForRange(isEndDateLessThanToday, endDate, todayDate, true);
					}else {
						validEndDate = getValidDateForRange(isEndDateLessThanToday, endDate, startDate, true);
					}
				}
				else if (!ctrl.pxblockPastDates && ctrl.pxblockFutureDates){
					if (isEndDateLessThanToday){
						validStartDate = getValidDateForRange(isStartDateGreaterThanToday, startDate, endDate, false);
					}else {
						if (isEndDateGreaterThanToday){
							validEndDate = todayDate;
							validStartDate = getValidDateForRange(isStartDateGreaterThanToday, startDate, todayDate, false);
						}
					}
				}
			}
			setRangeProperty(validStartDate, validEndDate);
		};
		
		/* Managing of date parameters to import  */
		var manageImportedParams = function(context){
			var SUFFIX_IMPORTED_PARAMS = '_PARAM_',
				startDateToImport		= getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + PARAM_START_DATE]),
				startDateEpochToImport 	= getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + PARAM_START_EPOCH_DATE]),
				endDateToImport 		= getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + PARAM_END_DATE]),
				endDateEpochToImport	= getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + PARAM_END_EPOCH_DATE]), 
				startDateValidToImport	= getValidDatesToImport(startDateToImport, startDateEpochToImport),
				endDateValidToImport	= getValidDatesToImport(endDateToImport, endDateEpochToImport);
				
			manageRangeProperty(startDateValidToImport, endDateValidToImport);
		};
		
		/* Watch on widget context */
		var listenerOnContextChange = function() {
			$scope.$watch('DECISYON.target.content.ctx', function(newContext, oldContext) {
				if (!angular.equals(newContext, oldContext)) {
				manageOptions(newContext);
				manageImportedParams(newContext);
				}
			},true);
		};
		
		//Initialize widget
		ctrl.initialize = function(){
			manageOptions(ctx);
			manageImportedParams(ctx);
			applyDomListeners();
			listenerOnContextChange();
		};
	};
	
	PxRangePickerCtrl.$inject = ['$scope', '$timeout', '$element'];

	DECISYON.ng.register.controller('pxRangePickerCtrl', PxRangePickerCtrl);
}());