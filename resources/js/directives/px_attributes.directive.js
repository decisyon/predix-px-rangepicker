(function() {
	'use strict';

	var PxAttributes = function (){
		return {
			restrict	: 'A',
			link: function (scope, element, attrs) {
				var prefixToSearch = 'px-',
					toCamelCase = function (str) {
					  // Lower cases the string
					  return str.toLowerCase()
					    // Replaces any - or _ characters with a space
					    .replace( /[-_]+/g, ' ')
					    // Removes any non alphanumeric characters
					    .replace( /[^\w\s]/g, '')
					    // Uppercases the first character in each group immediately following a space
					    // (delimited by spaces)
					    .replace( / (.)/g, function($1) { return $1.toUpperCase(); })
					    // Removes spaces
					    .replace( / /g, '' );
					};
					
                var isValidValue = function(value){
                    return (value !== 'false' && value !== '');
                };
                
				angular.forEach(attrs.$attr, function(key, value){
					if(key.indexOf('px-') === 0 && !angular.equals(key, 'px-attrs')){
						var attributeToSet = key.substr(prefixToSearch.length, key.length);
						var namespaceAttribute = toCamelCase(key);

						if(isValidValue(attrs[namespaceAttribute])){
							element.attr(attributeToSet, attrs[toCamelCase(key)]);
						}

						attrs.$observe(namespaceAttribute, function(value) {
							if(isValidValue(value)){
								element.attr(attributeToSet, value);
							}
							else{
								element.removeAttr(attributeToSet);
							}
						});
					}
				});


			}
		};
	};

	DECISYON.ng.register.directive('pxAttributes', PxAttributes);
}());